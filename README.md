## Need Matchmaking Laravel Front END

### Owner
- #team-needs-matchmaking

### Prerequisites
- [Git](https://www.atlassian.com/git/tutorials/install-git)
- PHP => 7.2.5

## Local Development Server

You can use either Homestead or Valet

- [Homestead](https://laravel.com/docs/7.x/homestead)
- [Valet](https://laravel.com/docs/7.x/valet)

### Getting started for local development
- Install the [prerequisites](#prerequisites)

- Clone this repository
    ```sh
    git clone git@gitlab.com:kawalcovid19/needs-matchmaking/backend.git
    ```

- Copy `.env` from `env.sample` and modify the configuration value appropriately

- Install PHP Packages
    ```sh
    composer install
    ```
- Install NPM package & compile
    ```sh
    npm i && npm run dev
    ```

### To Contribute
- Find Open ticket and Issues list
- create branch name with prefix or suffix as ticket name
- run make help: to get information
- create Merge Request and wait at least 1 reviewer

Thank you for contributing
