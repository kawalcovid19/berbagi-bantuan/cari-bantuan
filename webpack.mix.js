const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css', {
        sassOptions: {
            includePaths:  ['./node_modules']
        }
    });

mix.styles([
    'resources/css/global.css',
    'resources/css/index.css',
], 'public/css/index.css');

mix.styles([
    'resources/css/global.css',
    'resources/css/home.css',
], 'public/css/home.css');

mix.styles([
    'resources/css/global.css',
    'resources/css/form.css',
], 'public/css/form.css');

mix.scripts([
    'resources/js/index.js',
], 'public/js/index.js');

mix.scripts([
    'resources/js/home.js',
], 'public/js/home.js');

mix.scripts([
    'resources/js/volunteer/index.js',
], 'public/js/volunteer/index.js');

mix.scripts([
    'resources/js/form.js',
], 'public/js/form.js');

if (mix.inProduction()) {
    mix.version();
}
