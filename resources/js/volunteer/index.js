$(document).ready(function () {
    let map, infoWindow, markers, iWindowStatus, contributors, beneficiaries;
    let longitude = 106.8249641;
    let latitude = -6.1753871;
    infoWindow = new google.maps.InfoWindow();
    let mapStyle = [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#212121"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#212121"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "administrative.country",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#181818"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#1b1b1b"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#2c2c2c"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#8a8a8a"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#373737"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#3c3c3c"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#4e4e4e"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#3d3d3d"
                }
            ]
        }
    ];
    let activeTab = 'pencari-tab';

    initMap();
    // fetchBeneficiary();
    // fetchContributors();

    $(document).on("click", "div.jsItem", function () {
        displayInfoWindowOnMap($(this))
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        activeTab = e.target.id;
    });

    $('.jsSearch').on('click', function () {
        if (activeTab === 'pencari-tab') {
            fetchBeneficiary();
        } else {
            fetchContributors();
        }
    });

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: latitude, lng: longitude},
            zoom: 15,
            disableDefaultUI: true,
            styles: mapStyle
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                latitude = position.coords.latitude;
                longitude = position.coords.longitude;

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(map);
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        fetchBeneficiary();
        fetchContributors();
    }

    function fetchContributors() {
        $('#contributorData').html('');
        $.ajax({
            url: `/volunteer/contributor/get`,
            context: self,
            data: { lat: latitude, lng: longitude, term: $('#term').val(), q: $('input[name="query"]').val() },
            method: 'GET'
        }).done(function (responses) {
            displayList(responses.data, 'contributor');
            setMapOnAll(responses.data, 'contributor');
            contributors = responses.data;
        }).fail(function (error) {
            console.log(error);
        });
    }

    function fetchBeneficiary() {
        $('#beneficiaryData').html('');
        $.ajax({
            url: `/volunteer/beneficiary/get`,
            context: self,
            data: { lat: latitude, lng: longitude, term: $('#term').val(), q: $('input[name="query"]').val() },
            method: 'GET'
        }).done(function (responses) {
            displayList(responses.data, 'beneficiary');
            setMapOnAll(responses.data, 'beneficiary');
            beneficiaries = responses.data;
        }).fail(function (error) {
            console.log(error);
        });
    }

    function displayList(data, type) {
        for (let i = 0; i < data.length; i++) {
            if (type === 'contributor') {
                let contributorTemplate =
                    `
                        <div class="col-md-12 kwl-card-contributor mb-2 jsItem" id="item_${i}" data-type="contributor">                    
                            <div class="kwl-card-title">${data[i]['name']} ${data[i]['provider_name'] ? '<span class="text-info"><i class="material-icons" style="font-size: 14px !important;">check_circle</i></span>' : ''}</div> 
                       
                            <div class="kwl-card-name text-capitalize">Oleh ${data[i]['provider_name']}</div>
                            <span class="text-white" style="position: absolute;right: 2%;bottom: 2%">${Math.ceil(data[i]['distance']['length'])}${data[i]['distance']['unit']}</span>
                        </div>
                    `
                ;
                $('#contributorData').append(contributorTemplate);
            } else {
                let beneficiaryTemplate =
                    `
                        <div class="col-md-12 kwl-card-beneficiary mb-2 jsItem" id="item_${i}" data-type="beneficiary">                    
                            <div class="kwl-card-title">${data[i]['name']} ${data[i]['provider_name'] ? '<span class="text-info"><i class="material-icons" style="font-size: 14px !important;">check_circle</i></span>' : ''}</div>
                            <div class="kwl-card-name">${data[i]['receiver_name']}</div>              
                            ${data[i]['provider_name'] !== ' ' ? `<div class="kwl-card-status-verified">Telah terverifikasi oleh ${data[i]['provider_name']}</div>` : '<div class="kwl-card-status-unverified">Belum diverifikasi</div>'}
                            <span class="text-white" style="position: absolute;right: 2%;bottom: 2%">${Math.ceil(data[i]['distance']['length'])}${data[i]['distance']['unit']}</span>
                        </div>
                    `
                ;
                $('#beneficiaryData').append(beneficiaryTemplate);
            }
        }
    }

    function setMapOnAll(locations, type) {
        if (locations) {
            for (let i = 0; i < locations.length; i++) {
                createMarker(locations[i], markerIcon(type), type);
            }

        } else {
            for (let i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }
    }

    function createMarker(location, icon, type) {
        let geo = {
            lat: parseFloat(location['location']['latitude']),
            lng: parseFloat(location['location']['longitude'])
        };
        let newMarker = new google.maps.Marker({
            position: geo,
            map: map,
            icon: {
                url: icon,
                scaledSize: new google.maps.Size(32, 32)
            }
        });
        google.maps.event.addListener(newMarker, 'click', function(e) {
            infoWindow.setContent(makeInfoWindow(location), type);
            infoWindow.open(map, newMarker);
        });
    }

    function makeInfoWindow(populateData, type = 'beneficiary') {

        let items = populateData['item_details'];

        function kebutuhan() {
            var tableBody = [];
            for (var i = 0; i < items.length; i++) {
                var tembody = `<li><span class="kwl-infowindow-bold">${items[i]['name']}</span> <span class="kwl-infowindow-bold float-right">${items[i]['quantity']}</span></li>`;
                tableBody.push(tembody);
            }
            return tableBody.join('');
        }

        function statusBeneficiary(provider_name) {
            if (provider_name !== ' ') {
                return `<span class="kwl-verified-beneficiary-text">Pencari bantuan telah diverifikasi oleh ${provider_name}</span>`;
            }
            return `<span class="kwl-unverified-beneficiary-text">Pencari bantuan belum diverifikasi</span>`;
        }

        let beneficiaryTemplate =
            `
             <div class="kwl-infowindow-container">
                <h6 class="text-center mb-4">${populateData['name']}</h6>
                <dl>
                    <dt>Status</dt>
                    <dd>${statusBeneficiary(populateData['provider_name'])}</dd>
              
                    <dt>Keterangan</dt>
                    <dd>${populateData['note']}</dd>
              
                    <dt>Nama Pencari Bantuan</dt>
                    <dd>${populateData['receiver_name']}</dd>
              
                    <dt>Alamat</dt>
                    <dd>${populateData['location']['address']}</dd>
                </dl>

                <ul class="list-unstyled">
                    ${kebutuhan()}
                </ul>
                <hr>
                <div class="row mb-2">
                    <div class="col-md-6">
                        ${populateData['is_whatsapp_available'] === true ? `<a href="tel://${populateData['phone']}" class="btn btn-primary active btn-block kwl-btn-hubungi">Hubungi</a>` : ``}
                    </div>
                <div class="col-md-6">
                    <a href="https://www.google.com/maps/search/?api=1&query=${populateData['location']['latitude']},${populateData['location']['longitude']}"
                    target="_blank" class="btn btn-info active btn-block kwl-btn-petunjuk">Petunjuk Arah</a>
                </div>
            </div>
        </div>
            `;

        function statusContributor () {

        }

        let contributorTemplate =
            `
             <div class="kwl-infowindow-container">
                <div class="mb-4">
                    <h6 class="text-center">${populateData['name']}</h6>
                    <div class="text-center kwl-verified-contributor-text"><i class="material-icons" style="font-size: 14px !important;">check_circle</i></span> Bantuan dari pengguna terverifikasi</div>
                </div>
                <dl>              
                    <dt>Keterangan</dt>
                    <dd>${populateData['note']}</dd>
              
                    <dt>Nama / Kontak Pemberi Bantuan</dt>
                    <dd>${populateData['location']['name']}</dd>
              
                    <dt>Alamat</dt>
                    <dd>${populateData['location']['address']}</dd>
                    
                    <dt>Nomor telepon</dt>
                    <dd>${populateData['phone']}</dd>
                </dl>

                <ul class="list-unstyled">
                    ${kebutuhan()}
                </ul>
                <hr>
                <div class="row mb-2">
                    <div class="col-md-6">
                        ${populateData['is_whatsapp_available'] === true ? `<a href="tel://${populateData['phone']}" class="btn btn-primary active btn-block kwl-btn-hubungi">Hubungi</a>` : ``}
                    </div>
                    <div class="col-md-6">
                        <a href="https://www.google.com/maps/search/?api=1&query=${populateData['location']['latitude']},${populateData['location']['longitude']}" 
                        target="_blank" class="btn btn-info active btn-block kwl-btn-petunjuk">Petunjuk Arah</a>
                    </div>
                </div>
            </div>
            `;

        if (type === 'beneficiary') {
            return beneficiaryTemplate;
        }
        return  contributorTemplate;
    }

    function displayInfoWindowOnMap(buttonInstance) {
        if (iWindowStatus) {
            infoWindow.close();
        }

        iWindowStatus = true;

        let data = buttonInstance.attr('id');
        let split_id = data.split("_");
        let index = Number(split_id[1]);
        let type = buttonInstance.attr('data-type');
        let populateData = contributors;

        let icon = 'https://berbagibantuan.kawalcovid19.id/images/marker_green.png';
        if (type === 'beneficiary') {
            populateData = beneficiaries;
            icon = 'https://berbagibantuan.kawalcovid19.id/images/marker_red.png';
        }

        let coordinate = {lat: parseFloat(populateData[index]['location']['latitude']), lng: parseFloat(populateData[index]['location']['longitude'])};

        let marker = new google.maps.Marker({
            position: coordinate,
            map: map,
            title: populateData[index]['location']['name'],
            icon: {
                url: icon,
                scaledSize: new google.maps.Size(32, 32)
            }

        });
        console.info(type);
        infoWindow.setContent(makeInfoWindow(populateData[index], type));
        infoWindow.open(map, marker);
    }

    function markerIcon(type) {
        if (type === 'contributor')
        {
            return 'https://berbagibantuan.kawalcovid19.id/images/marker_green.png';
        }
        return 'https://berbagibantuan.kawalcovid19.id/images/marker_red.png';
    }
});
