$(document).ready(function () {

    let map, markers, marker;

    let mapStyle = [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#212121"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#212121"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "administrative.country",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#181818"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#1b1b1b"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#2c2c2c"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#8a8a8a"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#373737"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#3c3c3c"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#4e4e4e"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#3d3d3d"
                }
            ]
        }
    ];

    let stepper = new Stepper($('.bs-stepper')[0]);

    $('#lanjutKeDetailBantuan').on('click', function () {
        if ($('#pac-input').val()) {
            stepper.next()
        } else {
            popup({
                title: 'Anda belum memilih lokasi',
                text: 'Mohon untuk memilih lokasi terlebih dahulu.',
            })
        }
    });

    $('#kembaliKePilihLokasi').on('click', function () {
        stepper.previous()
    });

    $('#kembaliKeDetailBantuan').on('click', function () {
        stepper.previous()
    });

    $('#lanjutKePreview').on('click', function () {
        if (validate()) {
            stepper.next();
            preview();
        }
    });

    $('#tambahCategory').on('click', function () {
        tambahCategory();
    });

    $('#simpan').on('click', function () {
        storeData();
    });

    $('.jsCancelInput').on('click', function () {
        swal.fire({
            title: 'Batalkan Pengisian',
            text: 'Jika membatalkan, Anda perlu mengisi formulir dari awal di kemudian hari',
            icon: 'warning',
            confirmButtonText: 'Ya, Batalkan',
            showCancelButton: true,
            cancelButtonText: 'Tidak, lanjutkan isi formulir',
            cancelButtonColor: '#D8232A',
        }).then((result) => {
            if (result.value) {
                window.location.href = $(this).attr('data-redirect')
            }
        });
    });

    initMap();

    function initMap() {
        map = new google.maps.Map(document.getElementById('map-pilih-lokasi'), {
            center: {
                lat: -2.548926,
                lng: 118.0148634
            },
            zoom: 5,
            mapTypeId: 'roadmap',
            disableDefaultUI: true,
            styles: mapStyle
        });

        infoWindow = new google.maps.InfoWindow;

        // NOTE: i think it is no need, please uncomment if it better to implement
        // // Try HTML5 geolocation.
        // if (navigator.geolocation) {
        //     navigator.geolocation.getCurrentPosition(function(position) {
        //         var pos = {
        //             lat: position.coords.latitude,
        //             lng: position.coords.longitude
        //         };
        //         infoWindow.setPosition(pos);
        //         infoWindow.setContent('Location found.');
        //         infoWindow.open(map);
        //         map.setCenter(pos);
        //     }, function() {
        //         handleLocationError(true, infoWindow, map.getCenter());
        //     });
        // } else {
        //     // Browser doesn't support Geolocation
        //     handleLocationError(false, infoWindow, map.getCenter());
        // }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        markers = [];

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {

            var places = searchBox.getPlaces();
            if (places.length === 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {

                let addressComponent = place['address_components'];
                for (let i = 0; i < addressComponent.length; i++) {
                    if (addressComponent[i]['types'].length >= 2) {
                        if (addressComponent[i]['types'][0] === 'administrative_area_level_1') {
                            document.getElementById("city").value = place['address_components'][i]['long_name'];
                        }

                        if (addressComponent[i]['types'][0] === 'country') {
                            document.getElementById("country").value = place['address_components'][i]['long_name'];
                        }
                    }
                }

                document.getElementById("alamat").value = place['formatted_address'];
                document.getElementById("lng").value = place.geometry.location.lng();
                document.getElementById("lat").value = place.geometry.location.lat();

                if (!place.geometry) {
                    return;
                }

                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                marker = new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    draggable:true,
                    position: place.geometry.location
                });

                google.maps.event.addListener(marker, 'dragend', function(e) {
                    displayPosition(this.getPosition());
                });

                markers.push(marker);

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });

            map.fitBounds(bounds);
        });
    }

    function displayPosition(pos) {
        document.getElementById("lng").value = pos.lng();
        document.getElementById("lat").value = pos.lat();
    }

    // function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    //     infoWindow.setPosition(pos);
    //     infoWindow.setContent(browserHasGeolocation ?
    //         'Error: The Geolocation service failed.' :
    //         'Error: Your browser doesn\'t support geolocation.');
    //     infoWindow.open(map);
    // }

    $('#formBarang').on('click', '.jsHapusCategory', function () {
        const parentID = $(this).parent().parent().attr('id');
        $(`#${parentID}`).remove();
    });

    function tambahCategory() {
        let lastCategory = $('.jsKwlCategory:last').attr('id');
        var split_id = lastCategory.split("_");
        var nextIndex = Number(split_id[1]) + 1;

        const templateCategory = `
            <div class="jsKwlCategory row" id="category_${nextIndex}">
                <div class="col-md-6">
                    <input name="namaBarang[]" type="text" class="form-control kwl-input" placeholder="Nama barang" aria-label="Nama Barang">
                </div>
                <div class="col-md-5">
                    <input name="jumlahBarang[]" type="number" min="1" class="form-control kwl-input text-right" value="1" aria-label="Jumlah Barang">
                </div>
                <div class="col-md-1 float-right text-right">
                    <button type="button" class="btn btn-danger jsHapusCategory"><i class="material-icons">delete</i></button>
                </div>
            </div>
        `;


        $('#kwlKategoriContainer').append(templateCategory);
    }

    function preview() {
        const daftarBarang = $('#jsPreviewKebutuhan');
        daftarBarang.empty();

        let nomor_telp = $('input[name="nomor_telepon"]').val() || '-'
        if (window.location.href.split('/')[4] === 'contributor') {
            let append_text = $('#tampilkan_nomor_HP').is(':checked')
                ? 'Dapat dilihat pengguna lain'
                : 'Tidak dapat dilihat pengguna lain'

            nomor_telp += `<span class="text-danger ml-3">${append_text}</span>`
        }

        $('.jsPreviewJudul').html($('input[name="judul"]').val() || '-');
        $('.jsPreviewPencariBantuan').html($('input[name="pencari_bantuan"]').val() || '-');
        $('.jsPreviewAlamat').html($('input[name="alamat"]').val() || '-');
        $('.jsPreviewNomorTelepon').html(nomor_telp);
        $('.jsPreviewKeterangan').html($('textarea[name="keterangan"]').val() || '-');

        var namaBarang = $("input[name='namaBarang[]']").map(function(){return $(this).val();}).get();
        var jumlahBarang = $("input[name='jumlahBarang[]']").map(function(){return $(this).val();}).get();

        for(var i = 0; i < namaBarang.length;i++) {
            if (namaBarang[i] !== '') {
                let templateBarang = `<dt>${namaBarang[i]}</dt><dd>${jumlahBarang[i]}</dd>`;
                daftarBarang.append(templateBarang);
            }
        }

    }

    function storeData() {
        let submitForm = $("#formBarang");

        $.ajax({
            url: submitForm.attr('action'),
            context: self,
            data: submitForm.serialize(),
            method: 'POST'
        }).done(function (res) {
            popup({
                icon: 'success',
                title: submitForm.data('success-title'),
                text: submitForm.data('success-text'),
                onClose: () => {
                    let redirect = window.location.href.split('/')[3] === 'volunteer'
                        ? '/volunteer'
                        : '/';

                    location.replace(redirect);
                }
            })
        }).fail(function (error) {
            console.log(error);
        });
    }

    function validate() {
        const fieldRequired = $('#formBarang .required-field')

        let hasValue = []
        fieldRequired.find('input').each((_, input) => {
            if (input.name.split('[]').length <= 1) {
                if (!input.value) hasValue.push(false)
            }
        })

        if (!hasValue.every(v => v === true)) {
            popup({
                title: 'Anda belum selesai mengisi formulir',
                text: 'Mohon cek kembali formulir Anda dan isi semua kolom untuk melanjutkan ke halaman berikutnya.',
            })
        } else {
            if (!fieldRequired.find('div')[1].children[0].value) {
                popup({
                    title: 'Anda belum menambahkan jenis kebutuhan',
                    html: 'Mohon menambahkan minimal 1 barang yang Anda butuhkan di kolom <b>Rincian Kebutuhan</b>.',
                })

                return false
            }

            if (!$('#setuju_syarat_ketentuan').is(':checked')) {
                popup({
                    title: 'Anda belum menyetujui syarat dan ketentuan',
                    text: 'Mohon membaca dan menyetujui syarat dan ketentuan untuk melanjutkan ke halaman berikutnya.',
                })

                return false
            }

            return true
        }
    }

    function popup(opts) {
        swal.fire({
            icon: 'error',
            timer: 2500,
            showCloseButton: true,
            showConfirmButton: false,
            ...opts
        })
    }
});
