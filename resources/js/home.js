let allContributors = [];
const CONTAINER_ELEMENT = '#beneficiariesContainer'

$(document).ready(function() {
  if (!navigator.geolocation) {
    getList({ lat: -6.175393, long: 106.827021 })
  } else {
    function success(position) {
      const { latitude, longitude } = position.coords

      getList({ lat: latitude, long: longitude })
    }

    function error() {
      getList({ lat: -6.175393, long: 106.827021 })
    }

    navigator.geolocation.getCurrentPosition(success, error)
  }
})

function getList(params) {
  console.log(`TCL: getList -> params`, params)
  $.get('/list-contributor', params).then(({ data }) => {
    $(CONTAINER_ELEMENT).empty()

    if (!data.length) {
      displayEmptyState()
    } else {
      allContributors = data
      displayList(data)
    }
  }).catch(err => {
    console.error(err)

    displayEmptyState(`
      Terjadi kesalahan saat memuat data.
      Tetapi jangan khawatir, ini tidak akan lama.
    `)
  })
}

function displayEmptyState(
  message = `
    Saat ini belum ada pemberi bantuan di sekitar lokasi Anda.
    Tetapi jangan khawatir, masih banyak pemberi bantuan di lokasi lain yang bisa membantu Anda.
  `
) {
  const ajukanPermintaanBantuanUrl = `/ajukan-permintaan-bantuan`
  const ajukanPermintaanBantuan = `
    Jika Anda membutuhkan sesuatu,
    <a class="text-danger" href="${ajukanPermintaanBantuanUrl}">silakan ajukan permintaan bantuan</a>.
  `

  $(CONTAINER_ELEMENT).html(`
    <p class="box__empty">
      ${message}
      ${ajukanPermintaanBantuan}
    </p>
  `)
}

function displayList(items) {
  let template = items.map(({ name, distance, item_details, provider_name = '' }) => {
    let listItem = ''
    if (item_details) {
      const maxItem = 3
      const lenItem = item_details.length

      for (let i = 0; i < lenItem; i++) {
        if (i >= maxItem) break

        listItem += `${item_details[i].name}, `
      }

      if (lenItem > maxItem) {
        listItem += `+${lenItem - maxItem}`
      } else {
        listItem = listItem.replace(/,\s*$/, '')
      }
    }

    const verifiedIcon = provider_name.replace(/\s+/, '')
      ? `<i class="material-icons text-info text-right box__icon">check_circle</i>`
      : ``

    const providerName = provider_name.length <= 1
      ? ``
      : `oleh ${toTitleCase(provider_name)}`

    const distanceLength = Math.ceil(distance['length'])

    return distanceLength <= 10 && `
      <div class="card box">
        <div class="card-body">
          <div class="box--title mb-3px">
            ${name}
            ${verifiedIcon}
            <span class="box--title__distance">
              ${distanceLength} ${distance.unit}
            </span>
          </div>
          <p class="card-text text-secondary mb-3px">${listItem}</p>
          <p class="card-text text-secondary mb-3px">${providerName}</p>
        </div>
      </div>
    `
  })

  if (template[0] === false) {
    template = displayEmptyState()
  }

  $(CONTAINER_ELEMENT).append(template)
}

function toTitleCase(str) {
  str = str.replace(/_/g, ' ')

  return str.replace(
    /\w\S*/g,
    txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  )
}
