@extends('layouts.master')

@section('content')
    <x-form pov="volunteer" type="contributor" actionRoute="storeVolunteerContributor" cancelRoute="indexVolunteer" successTitle="Bantuan Berhasil Didaftarkan!" successText="Pencari bantuan tidak akan melihat titik bantuan ini lagi.">

        <div class="form-group required-field">
            <label for="provider_name" class="kwl-form-label">Nama Organisasi</label>
            <select name="provider_name" id="provider_name" class="form-control kwl-select">
                @foreach($options as $item)
                    <option value="{{ $item }}">{{ $item }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group required-field">
            <label for="judul" class="kwl-form-label">Judul</label>
            <input type="text" class="form-control kwl-input" placeholder="Contoh: Popok bayi dari Yayasan Kasih Ibu" name="judul" id="judul" aria-label="Judul">
        </div>

        <div class="form-group">
            <label for="keterangan" class="kwl-form-label">Keterangan</label>
            <textarea aria-label="keterangan" class="form-control kwl-input" name="keterangan" id="keterangan" cols="2" rows="4" placeholder="Contoh: Yayasann Kasih Ibu di Kota Semarang memiliki 30 popok bayi untuk disumbangkan"></textarea>
        </div>

        <div class="form-group required-field">
            <label for="pencari_bantuan" class="kwl-form-label">Nama Pemberi Bantuan</label>
            <input type="text" class="form-control kwl-input" placeholder="Contoh: Ana Budi Dharma" name="pencari_bantuan" id="pencari_bantuan" aria-label="Pencari Bantuan">
        </div>

        <div class="form-group required-field">
            <label for="nomor_telepon" class="kwl-form-label">Nomor Telepon</label>
            <input type="text" class="form-control kwl-input" placeholder="082123939" aria-label="Nomor Telepon" name="nomor_telepon">
        </div>

        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" aria-label="Tampilkan Nomor HP" name="tampilkan_nomor_HP" id="tampilkan_nomor_HP" value="1">
            <label class="custom-control-label text-white" for="tampilkan_nomor_HP">Izinkan pengguna terverifikasi untuk melihat nomor ini</label>
        </div>

        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" aria-label="Terhubung dengan WhatsApp" name="terhubung_dengan_whatsapp" id="terhubung_dengan_whatsapp" value="1">
            <label class="custom-control-label text-white" for="terhubung_dengan_whatsapp">Saya dapat dihubungi melalui WhatsApp</label>
        </div>

        <div id="kwlKategoriContainer" class="mt-4 required-field">
            <label for="namaBarang[]" class="kwl-form-label">Rincian bantuan</label>
            <div class="jsKwlCategory row" id="category_1" style="margin-bottom: 12px">
                <div class="col-md-6">
                    <input name="namaBarang[]" type="text" class="form-control kwl-input" placeholder="Nama barang" aria-label="Nama barang">
                </div>
                <div class="col-md-5">
                    <input name="jumlahBarang[]" type="number" min="1" class="form-control kwl-input text-right" value="1" aria-label="1">
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                <button type="button" class="btn btn-danger" id="tambahCategory">+ Tambah Barang</button>
            </div>
        </div>
    </x-form>
@endsection
