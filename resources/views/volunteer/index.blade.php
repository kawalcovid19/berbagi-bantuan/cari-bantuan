@extends('layouts.master')

@push('styles')
    <!-- Styles -->
    <link href="{{ mix('css/index.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid contributorMainContainer">
        <div class="row no-gutters h-100" id="beneficiariesContainer">
            <div class="col-md-3">
                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link text-danger active" id="pencari-tab" data-toggle="tab" href="#pencari" role="tab" aria-controls="pencari" aria-selected="true">
                            Pencari Bantuan
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-success" id="pemberi-tab" data-toggle="tab" href="#pemberi" role="tab" aria-controls="pemberi" aria-selected="false">
                            Pemberi Bantuan
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="pencari" role="tabpanel" aria-labelledby="pencari-tab">
                        <div class="row mt-2 px-4 position-absolute kwl-data-container" id="beneficiaryData">

                        </div>
                        <div class="row">
                            <div class="col-md-12 position-absolute fixed-bottom py-2 px-4" style="z-index: 100">
                                <a href="{{ route('createVolunteerBeneficiary') }}" class="btn btn-danger btn-block active">Input Pencari Bantuan</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pemberi" role="tabpanel" aria-labelledby="pemberi-tab">
                        <div class="row mt-2 px-4 position-absolute kwl-data-container" id="contributorData">
                        </div>
                        <div class="row">
                            <div class="col-md-12 position-absolute fixed-bottom py-2 px-4" style="z-index: 100">
                                <a href="{{ route('createVolunteerContributor') }}" class="btn btn-success btn-block active">Input Pemberi Bantuan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="w-100" style="position: absolute;z-index: 1000;top: 2%">
                    <div class="row no-gutters" style="padding-left: 0.5em;padding-right: 0.5em">
                        <div class="col-md-2">
                            <input aria-label="label" style="border-radius: 0.25rem 0 0 0.25rem;border:none;background-image: none"
                                   class="form-control kwl-input" type="text" placeholder="Cari berdasarkan" readonly>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <select style="border-radius: 0;border: none;background-color: #e9ecef;background-clip: unset"
                                        name="term" id="term" class="form-group kwl-select" aria-label="search_term">
                                    <option value="name">Nama</option>
                                    <option value="location">Lokasi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input style="border-radius: 0;border: none;background-clip: unset"
                                       aria-label="query" type="text" class="form-control kwl-input w-100" name="query" id="query" placeholder="Cari berdasarkan judul">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button style="border-bottom-left-radius: 0;border-top-left-radius: 0" type="button" class="btn btn-danger active mb-2 jsSearch" data-type="beneficiary">Cari</button>
                            <form action="{{ route('downloadVolunteer') }}" class="form-inline float-right">
                                <button class="btn active btn-primary">Download</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="mapContainer">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.maps')}}&libraries=places" defer></script>
    <script src="{{ mix('js/volunteer/index.js') }}" defer></script>
    <script>document.querySelector('meta[name="viewport"]').setAttribute('content', 'width=1024')</script>
@endpush
