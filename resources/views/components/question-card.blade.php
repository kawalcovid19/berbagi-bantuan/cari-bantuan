<div class="question-card {{ $type }}">
  <h4 class="question-card--title text-white">
    {{ $title }}
  </h4>
  <p class="question-card--desc text-secondary">
    {{ $slot }}
  </p>
  <a href="{{ $link }}" class="btn btn-large btn-rounded w-100 text-white bg-{{ $type }}">
    {{ $btnText }}
  </a>
</div>
