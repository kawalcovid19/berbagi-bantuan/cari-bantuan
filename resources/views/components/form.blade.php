@push('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
    <link rel="stylesheet" href="{{ mix('css/form.css') }}">
@endpush

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js" defer></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.maps') }}&libraries=places" defer></script>
    <script src="{{ mix('js/form.js') }}" defer></script>
@endpush

<div class="container">
    <div class="row h-100">
        <div class="col-md-12">
            <div class="mt-4">
                <h4 class="text-center text-white">
                    Daftarkan {{ $type == 'contributor' ? 'Bantuan' : 'Permintaan' }} Baru
                </h4>
            </div>
            <div class="bs-stepper">
                <div class="bs-stepper-header" role="tablist">
                    <div class="step" data-target="#pilih-lokasi">
                        <button type="button" class="step-trigger" role="tab" aria-controls="pilih-lokasi"
                                id="pilih-lokasi-trigger">
                            <span class="bs-stepper-circle">1</span>
                            <span class="bs-stepper-label">Pilih Lokasi</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#detail-bantuan">
                        <button type="button" class="step-trigger" role="tab" aria-controls="detail-bantuan"
                                id="detail-bantuan-trigger">
                            <span class="bs-stepper-circle">2</span>
                            <span class="bs-stepper-label">Isi Detail Bantuan</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#konfirmasi">
                        <button type="button" class="step-trigger" role="tab" aria-controls="konfirmasi"
                                id="konfirmasi-trigger">
                            <span class="bs-stepper-circle">3</span>
                            <span class="bs-stepper-label">Konfirmasi</span>
                        </button>
                    </div>
                </div>
                <div class="bs-stepper-content">
                    <div id="pilih-lokasi" class="content h-100" role="tabpanel" aria-labelledby="pilih-lokasi-trigger">
                        <div class="row mt-2">

                            <div class="col-md-12 mb-4" style="height: 500px;width: 100%">
                                <div class="h-100">
                                    <input aria-label="pac-input" id="pac-input" class="controls w-75 kwl-input" type="text" placeholder="Cari alamat">
                                    <div id="map-pilih-lokasi" class="h-100"></div>
                                </div>
                            </div>

                            <div class="col">
                                <button class="btn btn-warning active float-right" id="lanjutKeDetailBantuan">Pilih Lokasi</button>
                                <button class="btn text-white float-right jsCancelInput" data-redirect="{{ route($cancelRoute) }}">Batalkan</button>
                            </div>

                        </div>
                    </div>
                    <div id="detail-bantuan" class="content" role="tabpanel" aria-labelledby="detail-bantuan-trigger">
                        <div class="row">
                            <div class="col-md-12" style="min-height: 500px; width: 100%">
                                <form action="{{ route($actionRoute) }}" method="POST" id="formBarang" data-success-title="{{ $successTitle }}" data-success-text="{{ $successText }}">
                                    @csrf

                                    <input type="hidden" name="alamat" id="alamat">
                                    <input type="hidden" name="city" id="city">
                                    <input type="hidden" name="country" id="country">
                                    <input type="hidden" name="lat" id="lat">
                                    <input type="hidden" name="lng" id="lng">

                                    {{ $slot }}

                                    <div class="custom-control custom-checkbox mt-5">
                                        <input type="checkbox" class="custom-control-input" aria-label="Setuju Syarat Ketentuan" name="setuju_syarat_ketentuan" id="setuju_syarat_ketentuan">
                                        <label class="custom-control-label text-white" for="setuju_syarat_ketentuan">
                                            Saya telah menyetujui <a href="javascript:;" class="text-danger" data-toggle="modal" data-target="#exampleModal">syarat dan ketentuan</a>
                                        </label>
                                    </div>
                                </form>
                            </div>

                            <div class="col">
                                <button type="button" class="btn btn-warning active float-right" id="lanjutKePreview">Lanjutkan</button>
                                <button type="button" class="btn text-white float-right" id="kembaliKePilihLokasi">Kembali pilih lokasi</button>
                            </div>
                        </div>
                    </div>
                    <div id="konfirmasi" class="content" role="tabpanel" aria-labelledby="konfirmasi-trigger">
                        <div class="row" style="min-height: 500px;width: 100%">
                            <div class="col-md-12">
                                <dl class="text-white">
                                    <dt>Judul</dt>
                                    <dd class="jsPreviewJudul"></dd>

                                    <dt>Keterangan</dt>
                                    <dd class="jsPreviewKeterangan"></dd>

                                    <dt>{{ $type == 'contributor' ? 'Nama / Kontak Pemberi Bantuan' : 'Nama Pencari Bantuan' }}</dt>
                                    <dd class="jsPreviewPencariBantuan"></dd>

                                    <dt>Alamat</dt>
                                    <dd class="jsPreviewAlamat"></dd>

                                    <dt>Nomot Telepon</dt>
                                    <dd class="jsPreviewNomorTelepon"></dd>
                                </dl>
                            </div>

                            <div class="col-md-12">
                                <span class="text-white">
                                    {{ $pov == 'volunteer' ? 'Rincian Bantuan' : 'Rincian Barang yang Dibutuhkan' }}
                                </span>
                                <dl class="text-white" id="jsPreviewKebutuhan"></dl>
                            </div>

                            <div class="col">
                                <button type="button" class="btn btn-warning active float-right" id="simpan">
                                    Daftarkan {{ $type == 'contributor' ? 'Bantuan' : 'Permintaan' }}
                                </button>
                                <button type="button" class="btn text-white float-right" id="kembaliKeDetailBantuan">Kembali ke form</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">PERSYARATAN DAN KETENTUAN PENGGUNA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Tujuan dari pendaftaran nomor Whatsapp di dalam situs ini adalah agar KawalCOVID19 dapat menghubungi dan verifikasi data antara pemberi bantuan dan penerima bantuan (berupa bahan pokok/sembako/bantuan sosial) apabila diperlukan.</li>
                    <li>Pemberi bantuan dan penerima bantuan menjamin bahwa dalam memberikan data dan informasi terkait bantuan yang diberikan kepada KawalCOVID19 adalah benar dan akan mempertanggungjawabkan apabila data dan informasi yang diberikan tidak akurat.</li>
                    <li>Pengguna yang bukan atau pemberi/penerima bantuan bersedia mendaftarkan nomor Whatsapp di halaman <a class="text-danger font-weight-bold" href="https://berbagibantuan.kawalcovid19.id">berbagibantuan.kawalcovid19.id</a> dengan sadar dan tanpa ada paksaan dari pihak manapun.</li>
                    <li>Selain untuk menghubungi dan verifikasi Pengguna dalam pemberian/penerimaan bantuan, Pengguna mendaftarkan nomor Whatsapp di halaman <a class="text-danger font-weight-bold" href="https://berbagibantuan.kawalcovid19.id">berbagibantuan.kawalcovid19.id</a> karena ingin mendapatkan informasi tentang COVID-19 secara cepat dan akurat.</li>
                    <li>Pengguna bersedia menerima informasi yang dibagikan oleh KawalCOVID19 kepada saya melalui nomor Whatsapp yang didaftarkan.</li>
                    <li>Jika ada kesalahan atau ketidakakuratan informasi yang saya dapatkan atau temukan dari KawalCOVID19, saya akan menghubungi Admin KawalCOVID19 melalui <a class="text-danger font-weight-bold" href="https://kawalcovid19.id/">@KawalCOVID19</a>.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
