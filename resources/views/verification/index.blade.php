@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ mix('css/form.css') }}">
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if (session('error'))
                <div class="col-md-8 mt-5">
                    <div class="alert alert-danger" role="alert">
                        {{  session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            <div class="col-md-6 mt-5">
                <form action="{{ route('storeVerificationPage') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="password" class="kwl-form-label">Masukan Sandi</label>
                        <input type="password" name="password" class="form-control kwl-input" id="password">
                    </div>
                    <button class="btn btn-warning btn-block active">Lanjutkan</button>
                </form>
            </div>

        </div>
    </div>
@endsection
