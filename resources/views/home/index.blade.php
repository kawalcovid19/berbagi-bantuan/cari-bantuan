@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ mix('css/home.css') }}">
@endpush

@php
    $waMe = 'http://wa.me/' . env('PIC_WA');
    $createBeneficiary = route('createBeneficiary');
@endphp

@section('content')
<div class="container-fluid" id="beneficiariesWrapper">
    <div class="row">
        <div class="col-lg-3">
            <x-question-card
                type="danger"
                title="Anda Perlu Bantuan?"
                btnText="Ajukan Permintaan Bantuan"
                :link="$createBeneficiary"
            >
                Buat permintaan bantuan dan isikan kebutuhan Anda. Tim relawan akan menghubungi Anda apabila kebutuhan Anda telah tersedia dan dapat disalurkan.
            </x-question-card>

            <x-question-card
                type="success"
                title="Ingin Memberikan Bantuan?"
                btnText="Hubungi Tim Kami"
                :link="$waMe"
            >
                Hubungi tim kami untuk menyalurkan bantuan yang anda miliki. Tim kami akan mengarahkan cara pemberian bantuan.
            </x-question-card>
        </div>
        <div class="col-lg-9">
            <h4 class="text-white">Pemberi bantuan di sekitar Anda</h4>
            <div id="beneficiariesContainer">
                <p class="box__empty">Memuat data</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="{{ mix('js/home.js') }}" defer></script>
@endpush
