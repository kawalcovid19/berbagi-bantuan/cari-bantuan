<?php

namespace App\Models\Bantuan;

use Illuminate\Database\Eloquent\Model;

class Bantuan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bantuans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama_lokasi', 'alamat', 'lat', 'lng', 'showNoHP', 'noHP', 'kebutuhan'];

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setKebutuhanAttribute($value)
    {
        $this->attributes['kebutuhan'] = json_encode($value);
    }
}
