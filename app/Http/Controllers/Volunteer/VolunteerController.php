<?php

namespace App\Http\Controllers\Volunteer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VolunteerController extends Controller
{
    public function index()
    {
        return view('volunteer.index');
    }

    public function download()
    {
        $token = session()->get('token');
        $contributors = $this->backendInstance()->getListBeneficiary($token, 1000, false, null, null, null);

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file_export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $csv = $this->makeCSV($contributors->data);
        return response()->stream($csv, 200, $headers);
    }

    private function makeCSV($data)
    {
        $columns = ['nama', 'alamat', 'No HP', 'keterangan'];
        $callback = function() use ($data, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($data as $item) {
                $itemDetails = [];
                foreach ($item->item_details as $val) {
                    $itemDetails[] = $val->name.': '.$val->quantity;
                }

                fputcsv($file, [$item->receiver_name, $item->location->address, $item->phone, $item->phone, implode($itemDetails)]);
            }
            fclose($file);
        };
        return $callback;
    }
}
