<?php

namespace App\Http\Controllers\Volunteer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VolunteerBeneficiaryController extends Controller
{
    public function create()
    {
        $options = explode(',', config('services.organizations'));
        $this->addParam('options', $options);
        return view('volunteer.beneficiary.create', $this->viewParam);
    }

    public function get(Request $request)
    {
        $query = $request->q;
        if ($request->term == 'nama') {
            $beneficiaries = $this->backendInstance()->getListBeneficiary('50',true, $request->lat, $request->lng, $query, '');
        } else {
            $beneficiaries = $this->backendInstance()->getListBeneficiary('50',true, $request->lat, $request->lng, '', $query);
        }
        return response()->json($beneficiaries, 200);
    }

    public function store(Request $request)
    {
        $barang = $request->namaBarang;
        $jumlah = $request->jumlahBarang;
        $stock = [];
        for($i = 0; $i < count($barang); $i++) {
            $stock[] = ['name' => $barang[$i], 'quantity' => floatval($jumlah[$i])];
        }

        $body = [
            'name' => $request->judul,
            'category' => ' ',
            'phone' => $request->nomor_telepon,
            'amount' => 1,
            'is_whatsapp_available' => $request->tampilkan_nomor_HP ? true : false,
            'note' => $request->keterangan,
            'receiver_name' => $request->pencari_bantuan,
            'provider_name' => $request->provider_name,
            'item_details' => $stock,
            'location' => [
                'name' => $request->pencari_bantuan,
                'address' => $request->alamat,
                'country' => $request->country,
                'city' => $request->city,
                'longitude' => floatval($request->lng),
                'latitude' => floatval($request->lat),
            ]
        ];

        try {
            $beneficiary = $this->backendInstance()->createBeneficiary($body);
            return response()->json($beneficiary, 200);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }
}
