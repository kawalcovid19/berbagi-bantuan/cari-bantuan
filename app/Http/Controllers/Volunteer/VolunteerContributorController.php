<?php

namespace App\Http\Controllers\Volunteer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VolunteerContributorController extends Controller
{
    public function create()
    {
        $options = explode(',', config('services.organizations'));
        $this->addParam('options', $options);
        return view('volunteer.contributor.create', $this->viewParam);
    }

    public function get(Request $request)
    {
        $query = $request->q;
        if ($request->term == 'nama') {
            $contributors = $this->backendInstance()->getListContributor('50',true, $request->lat, $request->lng, $query, '');
        } else {
            $contributors = $this->backendInstance()->getListContributor('50',true, $request->lat, $request->lng, '', $query);
        }
        return response()->json($contributors, 200);
    }

    public function store(Request $request)
    {
        $barang = $request->namaBarang;
        $jumlah = $request->jumlahBarang;
        $stock = [];
        for($i = 0; $i < count($barang); $i++) {
            $stock[] = ['name' => $barang[$i], 'quantity' => floatval($jumlah[$i])];
        }

        $body = [
            'name' => $request->judul,
            'category' => ' ',
            'phone' => $request->nomor_telepon,
            'amount' => 1,
            'is_whatsapp_available' => $request->terhubung_dengan_whatsapp ? true : false,
            'is_phone_number_hidden' => $request->tampilkan_nomor_HP ? true : false,
            'note' => $request->keterangan,
            'provider_name' => $request->provider_name,
            'item_details' => $stock,
            'location' => [
                'name' => $request->pencari_bantuan,
                'address' => $request->alamat,
                'country' => $request->country,
                'city' => $request->city,
                'longitude' => floatval($request->lng),
                'latitude' => floatval($request->lat),
            ]
        ];

        try {
            $beneficiary = $this->backendInstance()->createContributor($body);
            return response()->json($beneficiary, 200);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }
}
