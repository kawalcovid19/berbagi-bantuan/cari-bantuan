<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VerificationPageController extends Controller
{
    public function index()
    {
        return view('verification.index');
    }

    public function store(Request $request)
    {
        if ($request->password == 'S3mogaB3rk4h')
        {
            session()->put('formAccess', rand());
            return redirect()->route('indexVolunteer');
        }
        return redirect()->back()->with('error', 'Maaf sandi yang Anda masukkan salah');
    }
}
