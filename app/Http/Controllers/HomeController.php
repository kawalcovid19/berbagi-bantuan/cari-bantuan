<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Get list contributor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $contributors = $this->backendInstance()->getListContributor('25', true, $request->lat, $request->long);
        return response()->json($contributors, 200);
    }

    /**
     * Submit a give help
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submitContributor(Request $request)
    {
        $barang = $request->namaBarang;
        $jumlah = $request->jumlahBarang;
        $stock = [];
        for($i = 0; $i < count($barang); $i++) {
            $stock[] = ['name' => $barang[$i], 'quantity' => floatval($jumlah[$i])];
        }

        $body = [
            'name' => $request->judul,
            'category' => ' ',
            'phone' => $request->nomor_telepon,
            'amount' => 1,
            'is_whatsapp_available' => $request->tampilkan_nomor_HP ? true : false,
            'is_phone_number_hidden' => $request->tampilkan_nomor_HP ? true : false,
            'note' => $request->keterangan,
            'provider_name' => ' ',
            'item_details' => $stock,
            'location' => [
                'name' => ' ',
                'address' => $request->alamat,
                'country' => $request->country,
                'city' => $request->city,
                'longitude' => floatval($request->lng),
                'latitude' => floatval($request->lat),
            ]
        ];

        try {
            $response = $this->backendInstance()->createContributor($body);
            return response()->json($response, 200);
        } catch (\Exception $exception) {
            info(print_r($exception->getMessage(), true));
            return response()->json($exception->getMessage(), 400);
        }
    }

    /**
     * Submit a request for help
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submitBeneficiary(Request $request)
    {
        $barang = $request->namaBarang;
        $jumlah = $request->jumlahBarang;
        $stock = [];
        for($i = 0; $i < count($barang); $i++) {
            $stock[] = ['name' => $barang[$i], 'quantity' => floatval($jumlah[$i])];
        }

        $body = [
            'name' => $request->judul,
            'category' => ' ',
            'phone' => $request->nomor_telepon,
            'amount' => 1,
            'is_whatsapp_available' => $request->tampilkan_nomor_HP ? true : false,
            'note' => $request->keterangan,
            'receiver_name' => $request->pencari_bantuan,
            'provider_name' => ' ',
            'item_details' => $stock,
            'location' => [
                'name' => ' ',
                'address' => $request->alamat,
                'country' => $request->country,
                'city' => $request->city,
                'longitude' => floatval($request->lng),
                'latitude' => floatval($request->lat),
            ]
        ];

        try {
            $response = $this->backendInstance()->createBeneficiary($body);
            return response()->json($response, 200);
        } catch (\Exception $exception) {
            info(print_r($exception->getMessage(), true));
            return response()->json($exception->getMessage(), 400);
        }
    }
}
