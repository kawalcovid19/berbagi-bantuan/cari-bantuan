<?php

namespace App\Services;

use GuzzleHttp\Client;

class TemuBantuanBackEnd
{
    protected $client;

    protected $apiEndpoint;

    protected $key;

    public function __construct()
    {
        $this->client = new Client();
        $this->apiEndpoint = config('services.backend.host');
        $this->key = config('services.backend.key');
    }

    public function createContributor($body)
    {
        $endpoint = $this->apiEndpoint.'/contributor';
        $param = [
            'headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key],
            'json' => $body
        ];

        try {
            $res = $this->client->post($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getListContributor($limit = 50, $find_nearby = false, $latitude = '', $longitude = '', $name = '', $address = '')
    {
        $endpoint = $this->apiEndpoint.'/contributor';
        $param = [
            'headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key],
            'query' => [
                'limit' => $limit,
                'find_nearby' => $find_nearby,
                'latitude' => floatval($latitude),
                'longitude' => floatval($longitude),
                'name' => $name,
                'location.address' => $address
            ]
        ];

        try {
            $res = $this->client->get($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getListBeneficiary($limit = 10, $find_nearby = false, $latitude = '', $longitude = '', $name = '', $address = '')
    {
        $endpoint = $this->apiEndpoint.'/beneficiary';
        $param = [
            'headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key],
            'query' => [
                'limit' => $limit,
                'find_nearby' => $find_nearby,
                'latitude' => floatval($latitude),
                'longitude' => floatval($longitude),
                'name' => $name,
                'location.address' => $address
            ]
        ];

        try {
            $res = $this->client->get($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getContributorDetail($id)
    {
        $endpoint = $this->apiEndpoint.'/contributor/'.$id;
        $param = ['headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key]];
        try {
            $res = $this->client->get($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getBeneficiaryDetail($id)
    {
        $endpoint = $this->apiEndpoint.'/beneficiary/'.$id;
        $param = ['headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key]];
        try {
            $res = $this->client->get($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function createBeneficiary($body)
    {
        $endpoint = $this->apiEndpoint.'/beneficiary';
        $param = [
            'headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key],
            'json' => $body
        ];
        try {
            $res = $this->client->post($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getListCategory($param)
    {
        // TODO add filters param
        $endpoint = $this->apiEndpoint.'/category';
        $param = ['headers' => ['Content-Type' => 'application/json', 'x-api-key' => $this->key]];

        try {
            $res = $this->client->get($endpoint, $param);
            return json_decode($res->getBody());
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
