<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Home Page Routes
|--------------------------------------------------------------------------
|
*/

Route::view('/', 'home.index')->name('home');
Route::view('/ajukan-permintaan-bantuan', 'home.create')->name('createBeneficiary');
Route::view('/beri-bantuan', 'home.contribute')->name('createContributor');
Route::get('/list-contributor', 'HomeController@list')->name('getContributor');
Route::post('/submit/beneficiary', 'HomeController@submitBeneficiary')->name('storeBeneficiary');
Route::post('/submit/contributor', 'HomeController@submitContributor')->name('storeContributor');

/*
|--------------------------------------------------------------------------
| Volunteer Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'verification-page'], function () {
    Route::get('', 'VerificationPageController@index')->name('indexVerificationPage');
    Route::post('store', 'VerificationPageController@store')->name('storeVerificationPage');
});

Route::group(['prefix' => 'volunteer', 'namespace' => 'Volunteer', 'middleware' => 'page'], function () {
    Route::get('', 'VolunteerController@index')->name('indexVolunteer');
    Route::get('download', 'VolunteerController@download')->name('downloadVolunteer');

    Route::group(['prefix' => 'beneficiary'], function () {
        Route::get('get', 'VolunteerBeneficiaryController@get')->name('getVolunteerBeneficiary');
        Route::get('create', 'VolunteerBeneficiaryController@create')->name('createVolunteerBeneficiary');
        Route::post('store', 'VolunteerBeneficiaryController@store')->name('storeVolunteerBeneficiary');
    });

    Route::group(['prefix' => 'contributor'], function () {
        Route::get('get', 'VolunteerContributorController@get')->name('getVolunteerContributor');
        Route::get('create', 'VolunteerContributorController@create')->name('createVolunteerContributor');
        Route::post('store', 'VolunteerContributorController@store')->name('storeVolunteerContributor');
    });
});
